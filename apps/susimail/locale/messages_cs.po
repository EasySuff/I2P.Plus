# I2P
# Copyright (C) 2009 The I2P Project
# This file is distributed under the same license as the susimail package.
# To contribute translations, see http://www.i2p2.de/newdevelopers
#
# Translators:
# A5h8d0wf0x <littleslyfoxie28@gmail.com>, 2014
# Pavel Pavel <tmp478523792@gmail.com>, 2016
# Waseihou Watashi <waseihou@gmail.com>, 2012
msgid ""
msgstr ""
"Project-Id-Version: I2P\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2022-03-07 02:50+0000\n"
"PO-Revision-Date: 2019-04-24 14:21+0000\n"
"Last-Translator: zzzi2p\n"
"Language-Team: Czech (http://www.transifex.com/otf/I2P/language/cs/)\n"
"Language: cs\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=4; plural=(n == 1 && n % 1 == 0) ? 0 : (n >= 2 && n "
"<= 4 && n % 1 == 0) ? 1: (n % 1 != 0 ) ? 2 : 3;\n"

#: src/src/i2p/susi/webmail/MailCache.java:529
#: src/src/i2p/susi/webmail/WebMail.java:2415
#, java-format
msgid "{0} new message"
msgid_plural "{0} new messages"
msgstr[0] ""
msgstr[1] ""
msgstr[2] ""
msgstr[3] ""

#: src/src/i2p/susi/webmail/MailCache.java:532
msgid "Email"
msgstr "E-mail"

#: src/src/i2p/susi/webmail/MailPart.java:321
#, java-format
msgid "No encoder found for encoding \\''{0}\\''."
msgstr "Nebyl nalezen žádný enkodér pro kódování \\''{0}\\''."

#. tagged in WebMail
#: src/src/i2p/susi/webmail/Sorters.java:124
#: src/src/i2p/susi/webmail/WebMail.java:1178
#: src/src/i2p/susi/webmail/WebMail.java:1179
msgid "Re:"
msgstr "Odpovědět:"

#: src/src/i2p/susi/webmail/Sorters.java:125
#: src/src/i2p/susi/webmail/WebMail.java:1258
#: src/src/i2p/susi/webmail/WebMail.java:1259
msgid "Fwd:"
msgstr "Přeposlat:"

#. MailDir-like
#: src/src/i2p/susi/webmail/WebMail.java:214
msgid "Drafts"
msgstr "Koncepty"

#. MailDir-like
#: src/src/i2p/susi/webmail/WebMail.java:215
msgid "Sent"
msgstr "Odesláno"

#. MailDir-like
#: src/src/i2p/susi/webmail/WebMail.java:216
msgid "Trash"
msgstr "Koš"

#. MailDir-like
#: src/src/i2p/susi/webmail/WebMail.java:217
msgid "Bulk Mail"
msgstr ""

#. untranslated, translate on use
#: src/src/i2p/susi/webmail/WebMail.java:221
msgid "Inbox"
msgstr "Doručené"

#: src/src/i2p/susi/webmail/WebMail.java:541
msgid "unknown"
msgstr "neznámý"

#: src/src/i2p/susi/webmail/WebMail.java:590
#, java-format
msgid "Charset \\''{0}\\'' not supported."
msgstr "Znaková sada \\''{0}\\'' není podporována."

#: src/src/i2p/susi/webmail/WebMail.java:594
#, java-format
msgid "Part ({0}) not shown, because of {1}"
msgstr "Část ({0}) nemohla být zobrazena, protože {1}"

#: src/src/i2p/susi/webmail/WebMail.java:595
msgid "Reloading the page may fix the error."
msgstr ""

#: src/src/i2p/susi/webmail/WebMail.java:625
msgid "File"
msgstr ""

#: src/src/i2p/susi/webmail/WebMail.java:648
#: src/src/i2p/susi/webmail/WebMail.java:653
#, java-format
msgid "Download attachment {0}"
msgstr "Stáhnou přílohu {0}"

#: src/src/i2p/susi/webmail/WebMail.java:654
msgid "File is packed into a zipfile for security reasons."
msgstr "Soubor je z bezpečnostních důvodů zazipován."

#: src/src/i2p/susi/webmail/WebMail.java:659
#, java-format
msgid "Attachment ({0})."
msgstr "Příloha ({0})."

#: src/src/i2p/susi/webmail/WebMail.java:717
msgid "Need username for authentication."
msgstr "K autentizaci je nutno zadat uživatelské jméno (username)."

#: src/src/i2p/susi/webmail/WebMail.java:721
msgid "Need password for authentication."
msgstr "K autentizaci je nutno zadat uživatelské jméno (password)."

#: src/src/i2p/susi/webmail/WebMail.java:725
msgid "Need hostname for connect."
msgstr "Pro připojení je nutno zadat jméno hosta (hostname)."

#: src/src/i2p/susi/webmail/WebMail.java:730
msgid "Need port number for pop3 connect."
msgstr "Zadejte číslo portu pro POP3."

#: src/src/i2p/susi/webmail/WebMail.java:737
msgid "POP3 port number is not in range 0..65535."
msgstr "Číslo portu pro POP3 musí být v rozmezí 0 až 65535."

#: src/src/i2p/susi/webmail/WebMail.java:743
msgid "POP3 port number is invalid."
msgstr "Číslo POP3 portu je neplatné."

#: src/src/i2p/susi/webmail/WebMail.java:749
msgid "Need port number for smtp connect."
msgstr "Zadejte číslo portup pro smtm."

#: src/src/i2p/susi/webmail/WebMail.java:756
msgid "SMTP port number is not in range 0..65535."
msgstr "Číslo portu pro SMTP musí být v rozmezí 0 až 65535."

#: src/src/i2p/susi/webmail/WebMail.java:762
msgid "SMTP port number is invalid."
msgstr "Číslo SMTP portu je neplatné."

#: src/src/i2p/susi/webmail/WebMail.java:839
#: src/src/i2p/susi/webmail/WebMail.java:1388
#: src/src/i2p/susi/webmail/pop3/POP3MailBox.java:446
#: src/src/i2p/susi/webmail/pop3/POP3MailBox.java:644
#: src/src/i2p/susi/webmail/pop3/POP3MailBox.java:657
#: src/src/i2p/susi/webmail/pop3/POP3MailBox.java:716
#: src/src/i2p/susi/webmail/pop3/POP3MailBox.java:754
#: src/src/i2p/susi/webmail/pop3/POP3MailBox.java:762
#: src/src/i2p/susi/webmail/smtp/SMTPClient.java:243
#: src/src/i2p/susi/webmail/smtp/SMTPClient.java:258
msgid "Cannot connect"
msgstr "Není možné se připojit"

#: src/src/i2p/susi/webmail/WebMail.java:935
#: src/src/i2p/susi/webmail/pop3/POP3MailBox.java:744
msgid "Error connecting to server"
msgstr "Chyba při připojování k serveru"

#: src/src/i2p/susi/webmail/WebMail.java:1042
msgid "Draft saved."
msgstr ""

#: src/src/i2p/susi/webmail/WebMail.java:1052
#: src/src/i2p/susi/webmail/WebMail.java:1293
#: src/src/i2p/susi/webmail/WebMail.java:2744
#: src/src/i2p/susi/webmail/WebMail.java:2927
msgid "Unable to save mail."
msgstr ""

#: src/src/i2p/susi/webmail/WebMail.java:1182
#, java-format
msgid "On {0} {1} wrote:"
msgstr "K {0} {1} napsal:"

#: src/src/i2p/susi/webmail/WebMail.java:1245
#: src/src/i2p/susi/webmail/WebMail.java:1470
#, java-format
msgid "Error reading uploaded file: {0}"
msgstr "Chyba při čtení nahraného souboru: {0}"

#: src/src/i2p/susi/webmail/WebMail.java:1271
msgid "begin forwarded mail"
msgstr "začátek přeposlaného mailu"

#: src/src/i2p/susi/webmail/WebMail.java:1281
msgid "end forwarded mail"
msgstr "konec přeposlaného mailu"

#: src/src/i2p/susi/webmail/WebMail.java:1299
#: src/src/i2p/susi/webmail/WebMail.java:3615
msgid "Could not fetch mail body."
msgstr "Nepodařilo se načíst obsah mailu."

#: src/src/i2p/susi/webmail/WebMail.java:1331
msgid "Message id not valid."
msgstr "Identifikátor zprávy není platný."

#: src/src/i2p/susi/webmail/WebMail.java:1375
msgid "Internal error, lost connection."
msgstr "Nastala vnitřní chyba, spojení bylo ztraceno."

#: src/src/i2p/susi/webmail/WebMail.java:1467
#: src/src/i2p/susi/webmail/smtp/SMTPClient.java:386
#, java-format
msgid "No Encoding found for {0}"
msgstr "Nebylo nalezeno kódování pro {0}"

#: src/src/i2p/susi/webmail/WebMail.java:1595
msgid "Attachment not found."
msgstr "Příloha nenalezena"

#. error if we get here
#: src/src/i2p/susi/webmail/WebMail.java:1623
#: src/src/i2p/susi/webmail/WebMail.java:1625
#: src/src/i2p/susi/webmail/WebMail.java:2313
#: src/src/i2p/susi/webmail/WebMail.java:3621
msgid "Message not found."
msgstr "E-mail nenalezen."

#: src/src/i2p/susi/webmail/WebMail.java:1728
#, java-format
msgid "1 message deleted."
msgid_plural "{0} messages deleted."
msgstr[0] "Počet smazaných zpráv: 1"
msgstr[1] "Počet smazaných zpráv: {0}"
msgstr[2] "Počet smazaných zpráv: {0}"
msgstr[3] "Počet smazaných zpráv: {0}"

#: src/src/i2p/susi/webmail/WebMail.java:1730
#: src/src/i2p/susi/webmail/WebMail.java:1739
msgid "No messages marked for deletion."
msgstr "Žádné zprávy nebyly označeny k smazání."

#: src/src/i2p/susi/webmail/WebMail.java:1803
#, java-format
msgid "Host unchanged. Edit configation file {0} to change host."
msgstr ""

#: src/src/i2p/susi/webmail/WebMail.java:1823
msgid "Configuration saved"
msgstr "Konfigurace uložena"

#: src/src/i2p/susi/webmail/WebMail.java:1845
msgid "Invalid pagesize number, resetting to default value."
msgstr "Neplatné hodnota velikosti stránky, použiji výchozí hodnotu."

#: src/src/i2p/susi/webmail/WebMail.java:2291
#: src/src/i2p/susi/webmail/WebMail.java:3166
msgid "Login"
msgstr "Login"

#: src/src/i2p/susi/webmail/WebMail.java:2293
#: src/src/i2p/susi/webmail/WebMail.java:2431
#: src/src/i2p/susi/webmail/WebMail.java:3183
msgid "Loading messages, please wait..."
msgstr ""

#: src/src/i2p/susi/webmail/WebMail.java:2301
#, fuzzy, java-format
msgid "1 message"
msgid_plural "{0} messages"
msgstr[0] "zpráv: 1"
msgstr[1] "zpráv: {0}"
msgstr[2] "zpráv: {0}"
msgstr[3] "zpráv: {0}"

#: src/src/i2p/susi/webmail/WebMail.java:2303
#: src/src/i2p/susi/webmail/WebMail.java:3384
msgid "No messages"
msgstr "Žádné zprávy"

#: src/src/i2p/susi/webmail/WebMail.java:2311
msgid "Show Message"
msgstr "Ukázat zprávu"

#: src/src/i2p/susi/webmail/WebMail.java:2316
msgid "New Message"
msgstr "Nová zpráva"

#: src/src/i2p/susi/webmail/WebMail.java:2318
msgid "Configuration"
msgstr "Konfigurace"

#: src/src/i2p/susi/webmail/WebMail.java:2329
msgid "I2PMail"
msgstr ""

#: src/src/i2p/susi/webmail/WebMail.java:2418
msgid "No new messages"
msgstr ""

#: src/src/i2p/susi/webmail/WebMail.java:2433
msgid "Checking for new messages on server"
msgstr ""

#: src/src/i2p/susi/webmail/WebMail.java:2435
msgid "Refresh the page for updates"
msgstr ""

#: src/src/i2p/susi/webmail/WebMail.java:2470
#, java-format
msgid "{0} is an I2P-exclusive service provided by {1}."
msgstr ""

#: src/src/i2p/susi/webmail/WebMail.java:2472
#, java-format
msgid "{0} webmail client &copy Susi 2004-2005."
msgstr ""

#: src/src/i2p/susi/webmail/WebMail.java:2652
#: src/src/i2p/susi/webmail/WebMail.java:3300
#: src/src/i2p/susi/webmail/WebMail.java:3590
msgid "no subject"
msgstr "žádný předmět"

#: src/src/i2p/susi/webmail/WebMail.java:2780
msgid "Found no valid sender address."
msgstr "Nebyla nalezena platná adresa odesílatele."

#: src/src/i2p/susi/webmail/WebMail.java:2786
#, java-format
msgid "Found no valid address in \\''{0}\\''."
msgstr "V \\''{0}\\'' nebyla nalezena platná adresa odesílatele."

#: src/src/i2p/susi/webmail/WebMail.java:2800
msgid "No recipients found."
msgstr "Nebyli nalezeni žádní příjemci."

#: src/src/i2p/susi/webmail/WebMail.java:2814
#: src/src/i2p/susi/webmail/smtp/SMTPClient.java:302
#, java-format
msgid "Email is too large, max is {0}"
msgstr ""

#: src/src/i2p/susi/webmail/WebMail.java:2859
#: src/src/i2p/susi/webmail/WebMail.java:2940
msgid "Sending mail."
msgstr ""

#: src/src/i2p/susi/webmail/WebMail.java:2864
#: src/src/i2p/susi/webmail/smtp/SMTPClient.java:254
#: src/src/i2p/susi/webmail/smtp/SMTPClient.java:346
#: src/src/i2p/susi/webmail/smtp/SMTPClient.java:351
msgid "Error sending mail"
msgstr "Chyba při odesílání emailu"

#: src/src/i2p/susi/webmail/WebMail.java:2898
msgid "Mail sent."
msgstr "Zpráva odeslána."

#: src/src/i2p/susi/webmail/WebMail.java:2987
msgid "Send"
msgstr "Odeslat"

#: src/src/i2p/susi/webmail/WebMail.java:2988
msgid "Save as Draft"
msgstr ""

#: src/src/i2p/susi/webmail/WebMail.java:2989
#: src/src/i2p/susi/webmail/WebMail.java:3409
#: src/src/i2p/susi/webmail/WebMail.java:3499
#: src/src/i2p/susi/webmail/WebMail.java:3673
msgid "Cancel"
msgstr "Zrušit"

#: src/src/i2p/susi/webmail/WebMail.java:3077
#: src/src/i2p/susi/webmail/WebMail.java:3251
#: src/src/i2p/susi/webmail/WebMail.java:3595
msgid "From"
msgstr "Od"

#: src/src/i2p/susi/webmail/WebMail.java:3080
#: src/src/i2p/susi/webmail/WebMail.java:3251
#: src/src/i2p/susi/webmail/WebMail.java:3597
msgid "To"
msgstr "Do"

#: src/src/i2p/susi/webmail/WebMail.java:3082
#: src/src/i2p/susi/webmail/WebMail.java:3600
msgid "Cc"
msgstr "Cc"

#: src/src/i2p/susi/webmail/WebMail.java:3084
msgid "Bcc"
msgstr "Bcc"

#: src/src/i2p/susi/webmail/WebMail.java:3086
#: src/src/i2p/susi/webmail/WebMail.java:3253
#: src/src/i2p/susi/webmail/WebMail.java:3602
msgid "Subject"
msgstr "Předmět"

#: src/src/i2p/susi/webmail/WebMail.java:3092
msgid "Add Attachment"
msgstr "Přidat přílohu"

#. TODO: reset button label to "add attachment" when no attachments are visible (currently counts attachments added per session)
#: src/src/i2p/susi/webmail/WebMail.java:3095
#, fuzzy
msgid "Add attachment"
msgstr "Přidat přílohu"

#: src/src/i2p/susi/webmail/WebMail.java:3102
msgid "Attachments"
msgstr "Přílohy"

#: src/src/i2p/susi/webmail/WebMail.java:3113
msgid "Delete selected attachments"
msgstr "Smazat vybrané přílohy"

#: src/src/i2p/susi/webmail/WebMail.java:3129
msgid "I2PMail Login"
msgstr ""

#: src/src/i2p/susi/webmail/WebMail.java:3133
msgid "User"
msgstr "Uživatel"

#: src/src/i2p/susi/webmail/WebMail.java:3134
#, fuzzy
msgid "Username"
msgstr "Uživatel"

#: src/src/i2p/susi/webmail/WebMail.java:3138
#: src/src/i2p/susi/webmail/WebMail.java:3139
msgid "Password"
msgstr "Heslo"

#: src/src/i2p/susi/webmail/WebMail.java:3141
msgid "Toggle password visibility"
msgstr ""

#: src/src/i2p/susi/webmail/WebMail.java:3147
msgid "Host"
msgstr "Host"

#: src/src/i2p/susi/webmail/WebMail.java:3152
msgid "POP3 Port"
msgstr "POP3 Port"

#: src/src/i2p/susi/webmail/WebMail.java:3157
msgid "SMTP Port"
msgstr "SMTP Port"

#: src/src/i2p/susi/webmail/WebMail.java:3166
msgid "Read Mail Offline"
msgstr "Číst emaily offline"

#: src/src/i2p/susi/webmail/WebMail.java:3167
#: src/src/i2p/susi/webmail/WebMail.java:3413
msgid "Settings"
msgstr "Nastavení"

#: src/src/i2p/susi/webmail/WebMail.java:3172
#, fuzzy
msgid "Learn about I2PMail"
msgstr "Více o I2P mailu"

#: src/src/i2p/susi/webmail/WebMail.java:3173
msgid "Create Account"
msgstr "Vytvořit účet"

#: src/src/i2p/susi/webmail/WebMail.java:3198
#: src/src/i2p/susi/webmail/WebMail.java:3523
msgid "New"
msgstr "Nová"

#: src/src/i2p/susi/webmail/WebMail.java:3211
msgid "Check Mail"
msgstr "Zkontrolovat mail"

#: src/src/i2p/susi/webmail/WebMail.java:3219
msgid "Refresh Page"
msgstr ""

#: src/src/i2p/susi/webmail/WebMail.java:3232
#, java-format
msgid "1 Message"
msgid_plural "{0} Messages"
msgstr[0] "zpráv: 1"
msgstr[1] "zpráv: {0}"
msgstr[2] "zpráv: {0}"
msgstr[3] "zpráv: {0}"

#: src/src/i2p/susi/webmail/WebMail.java:3232
#: src/src/i2p/susi/webmail/WebMail.java:3535
#: src/src/i2p/susi/webmail/WebMail.java:3676
msgid "Logout"
msgstr "Odhlásit"

#: src/src/i2p/susi/webmail/WebMail.java:3250
#: src/src/i2p/susi/webmail/WebMail.java:3594
msgid "Date"
msgstr "Datum"

#: src/src/i2p/susi/webmail/WebMail.java:3255
msgid "Size"
msgstr "Velikost"

#: src/src/i2p/susi/webmail/WebMail.java:3256
msgid "Mark for deletion"
msgstr "Označit pro smazání"

#: src/src/i2p/susi/webmail/WebMail.java:3353
msgid "Message has an attachment"
msgstr "Zpráva obsahuje přílohu"

#: src/src/i2p/susi/webmail/WebMail.java:3363
msgid "Message is new"
msgstr "Zpráva je nová"

#: src/src/i2p/susi/webmail/WebMail.java:3365
msgid "Message is spam"
msgstr "Zpráva je spam"

#: src/src/i2p/susi/webmail/WebMail.java:3374
#, fuzzy
msgid "Message body not downloaded"
msgstr "Identifikátor zprávy není platný."

#. TODO ngettext
#: src/src/i2p/susi/webmail/WebMail.java:3407
msgid "Really delete the marked messages?"
msgstr "Opravdu vymazat označené zprávy?"

#: src/src/i2p/susi/webmail/WebMail.java:3408
msgid "Yes, really delete them!"
msgstr "Ano, chci je opravdu vymazat!"

#: src/src/i2p/susi/webmail/WebMail.java:3414
msgid "Delete Selected"
msgstr "Smazat vybrané"

#: src/src/i2p/susi/webmail/WebMail.java:3415
msgid "Mark All"
msgstr "Označit vše"

#: src/src/i2p/susi/webmail/WebMail.java:3416
msgid "Clear All"
msgstr "Smazat vše"

#: src/src/i2p/susi/webmail/WebMail.java:3429
msgid "Change to Folder"
msgstr ""

#: src/src/i2p/susi/webmail/WebMail.java:3437
msgid "First"
msgstr "První"

#: src/src/i2p/susi/webmail/WebMail.java:3438
#: src/src/i2p/susi/webmail/WebMail.java:3547
msgid "Previous"
msgstr "Předchozí"

#: src/src/i2p/susi/webmail/WebMail.java:3447
#: src/src/i2p/susi/webmail/WebMail.java:3562
msgid "Next"
msgstr "Další"

#: src/src/i2p/susi/webmail/WebMail.java:3448
msgid "Last"
msgstr "Poslední"

#: src/src/i2p/susi/webmail/WebMail.java:3497
msgid "Really delete this message?"
msgstr "Opravdu chcete smazat tuto zprávu?"

#: src/src/i2p/susi/webmail/WebMail.java:3498
msgid "Yes, really delete it!"
msgstr "Ano, opravdu ji chci smazat!"

#: src/src/i2p/susi/webmail/WebMail.java:3526
msgid "Reply"
msgstr "Odpovědět"

#: src/src/i2p/susi/webmail/WebMail.java:3527
msgid "Reply All"
msgstr "Odpovědět všem"

#: src/src/i2p/susi/webmail/WebMail.java:3528
msgid "Forward"
msgstr "Přeposlat"

#: src/src/i2p/susi/webmail/WebMail.java:3529
msgid "Save As"
msgstr "Uložit jako"

#: src/src/i2p/susi/webmail/WebMail.java:3531
#: src/src/i2p/susi/webmail/WebMail.java:3533
msgid "Delete"
msgstr "Smazat"

#: src/src/i2p/susi/webmail/WebMail.java:3559
msgid "Back to Folder"
msgstr "Zpět do složky"

#: src/src/i2p/susi/webmail/WebMail.java:3580
msgid "Move to Folder"
msgstr ""

#: src/src/i2p/susi/webmail/WebMail.java:3660
msgid "Folder Page Size"
msgstr "Velikost stránky složky"

#: src/src/i2p/susi/webmail/WebMail.java:3661
msgid "Set"
msgstr "Nastavit"

#: src/src/i2p/susi/webmail/WebMail.java:3663
msgid "Advanced Configuration"
msgstr "Pokročilá konfigurace"

#: src/src/i2p/susi/webmail/WebMail.java:3674
msgid "Save Configuration"
msgstr "Uložit konfiguraci"

#. this appears in the UI so translate
#: src/src/i2p/susi/webmail/pop3/POP3MailBox.java:108
#: src/src/i2p/susi/webmail/pop3/POP3MailBox.java:644
#: src/src/i2p/susi/webmail/pop3/POP3MailBox.java:754
#: src/src/i2p/susi/webmail/pop3/POP3MailBox.java:936
#: src/src/i2p/susi/webmail/pop3/POP3MailBox.java:978
#: src/src/i2p/susi/webmail/pop3/POP3MailBox.java:1199
msgid "No response from server"
msgstr "Žádná odpověď od serveru"

#: src/src/i2p/susi/webmail/pop3/POP3MailBox.java:747
msgid "Mail server login failed, wrong username or password."
msgstr ""

#: src/src/i2p/susi/webmail/pop3/POP3MailBox.java:749
msgid "Logout and then login again with the correct username and password."
msgstr ""

#: src/src/i2p/susi/webmail/pop3/POP3MailBox.java:1238
#: src/src/i2p/susi/webmail/pop3/POP3MailBox.java:1240
#: src/src/i2p/susi/webmail/smtp/SMTPClient.java:314
msgid "Login failed"
msgstr "Přihlášení bylo neúspěšné"

#: src/src/i2p/susi/webmail/smtp/SMTPClient.java:256
#: src/src/i2p/susi/webmail/smtp/SMTPClient.java:285
msgid "Server refused connection"
msgstr "Server odmítá připojení"

#. TODO which recipient?
#: src/src/i2p/susi/webmail/smtp/SMTPClient.java:327
msgid "Mail rejected"
msgstr "Email byl odmítnut"

#~ msgid "User logged out."
#~ msgstr "Uživatel se odhlásil."

#~ msgid "Folder"
#~ msgstr "Složka"

#, java-format
#~ msgid "Page {0} of {1}"
#~ msgstr "Stránka {0}/{1}"
